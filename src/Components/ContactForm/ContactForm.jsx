import React from 'react'
import Swal from 'sweetalert2';
import { ContactSchema } from '../../Validation/ContactSchema';
import { useFormik } from 'formik';
import "./ContactForm.css";
import { baseURL } from '../../Functions/ScrollingToTop';

export default function ContactForm() {
  const onSubmit = async ( values , actions) => {
    const isValid = await ContactSchema.validate(values);
    if(isValid){
      const res = await fetch(`${baseURL}/contact-us`, {
        method: 'POST',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify(values)
      });
      const response = await res.json();
      if(response.success){
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: `${response.message}`,
          showConfirmButton: false,
          timer: 1000
        });
        actions.resetForm();
      }else {
        Swal.fire({
          icon: 'error',
          title: 'خطأ',
          text: `${response.message}`
        });
      }
    }
  };

  const { values , errors , touched , handleBlur , handleChange , handleSubmit } = useFormik({
    initialValues:{
      name: "",
      email: "",
      phone: "",
      subject: "",
      message: ""
    },
    validationSchema: ContactSchema,
    onSubmit,
  });

  return (
    <div className="contactForm">
      <div className='container'>
        <div className="row">
          <div className="col-lg-8 m-auto text-end contactForm__form">
            <h2 className='contactForm__heading'>تواصل معنا</h2>
            <form onSubmit={handleSubmit} onBlur={handleBlur} method='POST'>
              <div className={`mb-4`}>
                <input 
                className={`text-end form-control w-100 ${((errors.name && touched.name) )? "input-error" : ""}`}
                type="text" 
                name='name' 
                id='name'
                value={values.name}
                onChange={handleChange}
                placeholder='الاسم' 
                />
              </div>

              <div className={`mb-4`}>
                <input 
                className={`text-end form-control w-100 ${((errors.email && touched.email) )? "input-error" : ""}`}
                type="email" 
                name="email" 
                id="email"
                value={values.email}
                onChange={handleChange}
                placeholder='البريد الالكتروني'
                />
              </div>

              <div className={`mb-4`}>
                <input 
                className={`text-end form-control w-100 ${((errors.phone && touched.phone) )? "input-error" : ""}`}
                type="text"
                name="phone" 
                id="phone"
                value={values.phone}
                onChange={handleChange}
                placeholder='رقم الهاتف'
                />
              </div>

              <div className={`mb-4`}>
                <input 
                className={`text-end form-control w-100 ${((errors.subject && touched.subject) )? "input-error" : ""}`}
                type="text" 
                name="subject" 
                id="subject"
                value={values.subject}
                onChange={handleChange}
                placeholder='عنوان الرسالة'
                />
              </div>

              <div className={`mb-4`}>
                <textarea
                className={`text-end form-control w-100 ${((errors.message && touched.message) )? "input-error" : ""}`}
                name="message" 
                id="contactMessage"
                value={values.message}
                onChange={handleChange}
                placeholder='رسالتك'
                ></textarea>
              </div>

              <button 
              className='contact__mainSec__form__submitBtn btn' 
              type='submit'
              id="submitBtn"
              name='submitBtn'
              >
                ارسال
              </button>

            </form>
          </div>
        </div>
      </div>
    </div>
  )
}
