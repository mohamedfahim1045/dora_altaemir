export const scrollingToTop = (TOP) => {
    window.scrollTo({
        top: TOP
    });
};

export const baseURL = "https://backend.dora-altaemir.com/api";