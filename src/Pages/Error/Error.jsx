import React from 'react';
import "./Error.css";

export default function Error() {
  return (
    <div className='error__page-section'>
      <h1>Error-404</h1>
      <p>This page not found , Please try again later</p>
    </div>
  )
}
